if not hasattr(itertools, 'imap'):
    itertools.imap = map

if not hasattr(itertools, 'izip'):
    itertools.izip = zip

if not hasattr(itertools, 'ifilterfalse'):
    itertools.ifilterfalse = itertools.filterfalse
