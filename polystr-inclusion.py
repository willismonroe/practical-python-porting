# Any encoding that preserves 0x80...0x8f through round-tripping from byte
# streams to Unicode and back would do, latin-1 is the best known of these.

import io

binary_encoding = 'latin-1'

if str is bytes:  # Python 2
    
    polystr = str
    polybytes = bytes
    polyord = ord
    polychr = str

else:  # Python 3
    
    def polystr(o):
        if isinstance(o, str):
            return o
        if isinstance(o, bytes):
            return str(o, encoding=binary_encoding)
        raise ValueError

    def polybytes(o):
        if isinstance(o, bytes):
            return o
        if isinstance(o, str):
            return bytes(o, encoding=binary_encoding)
        raise ValueError

    def polyord(c):
        "Polymorphic ord() function"
        if isinstance(c, str):
            return ord(c)
        else:
            return c

    def polychr(c):
        "Polymorphic chr() function"
        if isinstance(c, int):
            return chr(c)
        else:
            return c

    def make_std_wrapper(stream):
        "Standard input/output wrapper factory function"
        # This ensures that the encoding of standard output and standard
        # error on Python 3 matches the binary encoding we use to turn
        # bytes to Unicode in polystr above
        
        # newline="\n" ensures that Python 3 won't mangle line breaks
        # line_buffering=True ensures that interactive command sessions work as expected
        return io.TextIOWrapper(stream.buffer, encoding=binary_encoding, newline="\n", line_buffering=True)

    sys.stdin = make_std_wrapper(sys.stdin)
    sys.stdout = make_std_wrapper(sys.stdout)
    sys.stderr = make_std_wrapper(sys.stderr)
